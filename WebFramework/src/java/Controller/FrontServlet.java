/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tiavina Ravaka
 */
public class FrontServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void init() {
        String[] packageNames =null;
        packageNames = this.getInitParameter("nomPakage").split("/");
        try {
            this.getServletContext().setAttribute("lien", Utilitaire.linkMapping(packageNames));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException , UrlNotSupportException{
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
                String url=Utilitaire.retriveUrlFromRawUrl(request.getRequestURI());
                HashMap<String, HashMap<String, String>> link = (HashMap<String, HashMap<String, String>>)this.getServletContext().getAttribute("lien");
            try {
                
                
                Utilitaire.urlIsPresent(url, link);
                HashMap<String,String> clsMth = link.get(url);
                
                ViewModel resultat=new ViewModel();
                
                Class cls = Class.forName(clsMth.get("class"));
                Method mth = cls.getDeclaredMethod(clsMth.get("method"));
                Object obj = cls.newInstance();
                Field[] flds = ReflectGenericMethod.getFields(obj, "get");
                
                Enumeration listAttr =  request.getParameterNames();
                Vector <String> listAttribute = new Vector<String>(1,1);
                while(listAttr.hasMoreElements()){
                    listAttribute.add((String)listAttr.nextElement());
                }
                if(listAttribute.size()>0){
                    for (String attr : listAttribute) {
                        try {

                            Field fld = cls.getDeclaredField(attr);
                            String attrVal = request.getParameter(attr);
                            Method set = ReflectGenericMethod.getMethod(cls, fld, "set");
                            if(fld.getType().getName().equalsIgnoreCase("java.lang.String")){
                                set.invoke(obj, fld.getType().cast(attrVal));
                            }
                            if(fld.getType().isPrimitive()){
                                if(fld.getType().getName().equalsIgnoreCase("int")){
                                    try {
                                        set.invoke(obj, Integer.parseInt(attrVal));
                                    } catch (Exception e) {
                                        out.println("incorrect");
                                    }
                                }else if(fld.getType().getName().equalsIgnoreCase("float")){
                                    try {
                                        set.invoke(obj, Float.parseFloat(attrVal));
                                    } catch (Exception e) {
                                        out.println("incorrect");
                                    }
                                }else if(fld.getType().getName().equalsIgnoreCase("double")){
                                    try {
                                        set.invoke(obj, Double.parseDouble(attrVal));
                                    } catch (Exception e) {
                                        out.println("incorrect");
                                    }
                                }
                                else if(fld.getType().getName().equalsIgnoreCase("Date")){
                                    try {
                                        set.invoke(obj, Date.valueOf(attrVal));
                                    } catch (Exception e) {
                                        out.println("incorrect");
                                    }
                                }
                            }
                        } catch (NoSuchFieldException ex) {

                            out.println(ex);
                            continue;
                        } 
                    }
                }
                
                resultat =(ViewModel) mth.invoke(obj);

                HashMap<String,Object> data = resultat.getData();
                for (Map.Entry<String, Object> entry : data.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    request.setAttribute(key, value);
                }
                String page=(resultat.getUrlRedirect());
                request.getRequestDispatcher("/"+page).forward(request, response);
            

            } catch (Exception e) {
                if(!link.containsKey(url)){
                            throw new UrlNotSupportException("URL not supported");
                  }
              
                else{
                    out.print(e.getMessage());
                }
            }        
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            
        } catch (UrlNotSupportException ex) {
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.println(ex.getMessage());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            processRequest(request, response);
            
        } 
        catch (UrlNotSupportException ex) {
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.println(ex.getMessage());
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}
